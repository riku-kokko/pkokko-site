variable "aws_access_key" {
}

variable "aws_secret_key" {
}

variable "site_domain" {
  default = "pkokko.fi"
}

provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = "eu-west-1"
}

resource "aws_s3_bucket" "pkokko-site" {
  bucket = var.site_domain
  acl    = "public-read"
  policy = file("bucket-policy.json")
  website {
    index_document = "index.html"
    error_document = "404.html"
  }
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["GET"]
    allowed_origins = [var.site_domain]
  }
}

resource "null_resource" "remove_and_upload_to_s3" {
  provisioner "local-exec" {
    command = "aws s3 sync ./public s3://${aws_s3_bucket.pkokko-site.id}"
    environment = {
      AWS_ACCESS_KEY_ID     = var.aws_access_key
      AWS_SECRET_ACCESS_KEY = var.aws_secret_key
    }
  }
  triggers = {
    index_sha = filesha1("public/index.html")
  }
}

## DOMAIN
