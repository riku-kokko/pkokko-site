#!/bin/bash
set -eu
npm run build
terraform apply
git add terraform.tfstate terraform.tfstate.backup
git commit -m "(deployment)"
