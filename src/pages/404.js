import React from 'react'
import './404.css'
import Layout from '../layout'

const NotFoundPage = () => (
  <Layout>
  <div id="hero">
    <h1>404</h1>
    <p>Valitettavasti etsimääsi sivua ei löytynyt</p>
  </div>
  </Layout>
)

export default NotFoundPage
