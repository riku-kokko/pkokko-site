import React, { Component } from 'react'
import { graphql } from 'gatsby'
import Layout from '../layout/'
import './index.css'
import viipurinLinnaMp4 from '../static/video/viipurin-linna.mp4'
import viipurinLinnaJpg from '../static/img/viipurin-linna.jpg'
import penttiKokkoJpg from '../static/img/pentti-kokko.jpg'
import tuntematonKopeJpg from '../static/img/tuntematon-kope.jpg'
import 'animate.css/animate.min.css'
import ScrollAnimation from 'react-animate-on-scroll'

class IndexPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      titleStyle: {
        transform: 'translate3d(0,0,0)',
      },
      titleShadowStyle: {
        transform: 'translate3d(2px,2px,0)',
      },
      heroVideoStyle: {
        transform: 'translate3d(0,0,0)',
      },
    }
    this.onScroll = this.onScroll.bind(this)
  }

  componentDidMount() {
    window.addEventListener('scroll', this.onScroll)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll)
  }

  onScroll(event) {
    this.moveHero(event)
  }

  moveHero(event) {
    let scrolled = window.scrollY
    const titleTranslate = -1 * scrolled * 0.5
    const titleShadowTranslate = titleTranslate + 2
    this.setState({
      heroVideoStyle: {
        transform: 'translate3d(0,-' + scrolled * 0.25 + 'px,0)',
      },
      titleStyle: { transform: 'translate3d(0,' + titleTranslate + 'px,0)' },
      titleShadowStyle: {
        transform: 'translate3d(2px,' + titleShadowTranslate + 'px,0)',
      },
    })
  }

  getBlogExcerpts() {
    return this.props.data.allMarkdownRemark.edges.map(edge => {
      return (
        <div className="section section-excerpt" key={edge.node.fields.slug}>
          <a href={edge.node.fields.slug}>
            <ScrollAnimation animateIn="fadeIn" animateOnce={true}>
              <h2>{edge.node.frontmatter.title}</h2>
              <div className="content">
                <div>
                  <p>
                    {edge.node.frontmatter.excerpt}{' '}
                    <span className="show-more">
                      <span>Lue lisää</span>
                      <span className="arrow arrow-right" />
                    </span>{' '}
                  </p>
                </div>
                <div className="content-pic">
                  <img
                    src={edge.node.frontmatter.image.childImageSharp.resize.src}
                    alt="Juttua kuvaava kuva"
                  />
                </div>
              </div>
            </ScrollAnimation>
          </a>
        </div>
      )
    })
  }

  render() {
    return (
      <Layout>
        <div id="main">
          <div className="overlay">
            <h1
              className="title title-shadow"
              style={this.state.titleShadowStyle}
            >
              Maisteri Pentti Kokko
            </h1>
            <h1 className="title" style={this.state.titleStyle}>
              Maisteri Pentti Kokko
            </h1>
            <video
              className="hero-vid"
              autoPlay
              loop
              muted
              poster={viipurinLinnaJpg}
              style={this.state.heroVideoStyle}
            >
              <source src={viipurinLinnaMp4} type="video/mp4" />
            </video>
          </div>
          <div className="container">
            <div className="section">
              <ScrollAnimation animateIn="fadeIn" animateOnce={true}>
                <div className="content">
                  <div>
                    <p>
                      Valtiotieteen maisteri Pentti Kokko on syntynyt Mikkelissä
                      vuonna 1947 ja valmistunut Helsingin yliopistosta 1971
                      pääaineenaan kansantaloustiede. Kokko toimi tilasto- ja
                      pankkialalla 1970-2010. Historian Ystäväin Liiton
                      hallituksen jäsen hän on ollut vuodesta 2011.
                    </p>
                  </div>
                  <div className="content-pic">
                    <img src={penttiKokkoJpg} alt="Pentti Kokko" />
                  </div>
                </div>
              </ScrollAnimation>
            </div>

            <div className="section">
              <ScrollAnimation animateIn="fadeIn" animateOnce={true}>
                <h2>Tuntematon kansakoulunopettaja</h2>
                <div className="content">
                  <div>
                    <p>
                      Tuntematon kansakoulunopettaja on Pentti Kokon
                      esikoisteos, joka käy läpi yhden suomalaisen
                      kansakoulunopettajan elämää. Kansakoulunopettaja Taavi
                      Kokko oli aktiivinen yhteiskunnallinen vaikuttaja. Hän
                      tarttui hanakasti epäkohtiin ja edisti määrätietoisesti
                      oikeaksi kokemiaan asioita niin kotikonnuillaan Karjalassa
                      kuin koko maassa.
                    </p>
                    <p>
                      Laajaa lähdeaineistoa peraten Pentti Kokko seuraa
                      isoisänsä elämäntaivalta ja piirtää luonteikkaan
                      henkilökuvan tarmokkaasta miehestä, jonka lukuisat
                      sanomalehtikirjoitukset luotasivat ajan ilmiöitä ja
                      yleistä elämänmenoa. Teos laajenee osaksi Suomen historiaa
                      1860-luvun lopun nälkävuosista ensimmäisten
                      eduskuntavaalien jälkeiseen kesään 1907.
                    </p>

                    <p>
                      Pentti Kokko: Tuntematon kansakoulunopettaja. Taavi Kokko
                      Luumäeltä Ruskealaan 1865–1907. Osaelämäkerta.
                      Omakustanne. Tallinna Raamatutrükikoda 2016. Kovissa
                      kansissa on 464 sivua hyvälaatuisesta paperista. Kirjassa
                      on viiteapparaatti, lähdeluettelo ja yli 550 nimen
                      henkilöhakemisto.
                    </p>
                    <h3>Tilaa Tuntematon kansakoulunopettaja</h3>
                    <p>
                      Kirjan voi tilata sähköpostitse osoitteesta{' '}
                      <a href="mailto:pentti.o.kokko@gmail.com">
                        pentti.o.kokko@gmail.com
                      </a>{' '}
                      tai puhelimitse numerosta 050 568 0566.
                    </p>
                    <p>
                      Hinta on 50 € + toimituskulut 10 €. Helsingin ja Mikkelin
                      seuduilla toimitus mahdollisuuksien mukaan kuluitta
                      ostajan kanssa sovittavaan paikkaan.
                    </p>
                  </div>
                  <div className="content-pic">
                    <img
                      src={tuntematonKopeJpg}
                      alt="Tuntematon kansakoulunopettaja kansikuva"
                    />
                  </div>
                </div>
              </ScrollAnimation>
            </div>
            {this.getBlogExcerpts()}
          </div>
        </div>
      </Layout>
    )
  }
}

export default IndexPage

export const query = graphql`
  query posts {
    allMarkdownRemark(sort: { order: DESC, fields: [frontmatter___date] }) {
      edges {
        node {
          frontmatter {
            title
            date
            excerpt
            image {
              childImageSharp {
                resize(width: 300, grayscale: true) {
                  src
                }
              }
            }
          }
          fields {
            slug
          }
        }
      }
    }
  }
`
