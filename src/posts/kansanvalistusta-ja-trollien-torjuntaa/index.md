---
title: 'Kansanvalistusta ja ”trollien” torjuntaa'
author: 'Professori Martti Häikiö'
date: '2019-01-03'
image: '../../static/img/stock-photos/sortavalan-seminaari.jpg'
excerpt: 'Professori Martti Häikiön kirja-arvio Tuntemattomasta kansakoulunopettajasta. Juttu on alunperin julkaistu Helsingin Suomalaisen Klubin Itsenäisyys100-sivustolla.'
disableSignature: true
keywords: 'Martti Häikiö, Taavi Kokko, Pentti Kokko, Tuntematon kansakoulunopettaja, Luumäki, Ruskeala, elämänkerta'
---

# Kansanvalistusta ja ”trollien” torjuntaa

Pentti Kokko: Tuntematon kansakoulunopettaja.<br/>
Taavi Kokko Luumäeltä Ruskealaan 1865–1907.<br/>
Omakustanne 2016.<br/>
464 sivua.<br/>

Laatokan pohjoispuolella sijaitsevassa Ruskealassa toiminut kansakoulunopettaja Taavi Kokko oli raittiusmies, maa- ja meijerimies, kirkkoväärti, kunnallis- ja suomalaisuusmies. Kaiken aikaa hän oli sanomalehtien kirjeenvaihtaja. Suomi nousi, kun miltei joka kylään tuli intomielinen ja monitoiminen opettaja karkottamaan tietämättömyyttä ja vanhoillisuutta. He kannustivat nuoria avartamaan maailmankuvaansa ja rakentamaan isänmaata. Kirjan alussa kuvataan Taavin kivistä tietä Luumäeltä Sortavalan seminaariin.

Pentti Kokko on dokumentoinut tarkasti isoisänsä Taavin elämän ja työn alkupuolen. Arjen rakentumisen ja aatteiden viriämisen kuvaus on mikrohistoriaa parhaimmillaan. Kiitos kuuluu tekijälle myös lähdeviitteistä ja henkilöhakemistosta, joista on iloa monille samojen seutujen, alojen ja aikojen tutkijoille.

Kirjassa kerrotaan myös yli sadan vuoden takaisista ”trolleista” eli tietoisen väärän tiedon levittäjistä. Taavi Kokko kirjoitti Laatokka-lehteen keväällä 1899:

> ”Kansankiihottajia liikkuu Ruskealassa. Ahkerin valheiden levittäjä on laukkuryssä Harju-Risto, joka on kotoisin sydän-Venäjältä. Talosta taloon kulkee hän, ja kertoo rahvaalle, että ’kohta tulee Suomessa voimaan Venäjän laki’, jonka mukaan ensinnä tapahtuu uusi maanjako, jossa kaikille irtolaisille annetaan omaa maata ja herroilta otetaan pois heidän suuret palkkansa ja siitä syystä kaikki ’herrat’ ovat vihoissaan uudelle asetukselle.”

Edelleen Taavi Kokko tiesi kertoa, että _”yksinkertainen kansa herkästi uskoo kaikenlaisia huhuja mieluummin kuin järkevää puhetta”_. Hän varoitti myös niistä _”kolmesta muukalaisesta, jotka Ruskealassa tätä nykyä kiertävät talosta taloon muka kerjäten. Ummikkovenäläisiä ovat olevinaan, mutta kuitenkin löytyy ihmisiä, jotka ovat kuulleet heidän suomea puhuvan”_. Huhujen levittäminen oli lähtöisin kenraalikuvernööri Bobrikovista ja ne olivat osa venäläisten psykologista sodankäyntiä suomalaisuutta puolustavan kansalaismielipiteen hajottamiseksi.

<div class="post__signature grayscale">
    <div>
        <img alt="Martti Häikiö" src="martti-häikiö.jpg" />
        <div>- Professori Martti Häikiö</div>
        <div>Helsingin Suomalainen Klubi</div>
        <div>Itsenäisyys100-sivusto</div>
        <div>Uusia Kirjoja</div>
    </div>
</div>
