---
title: 'Opettaja Taavi Kokon aikalaisia Ruskealassa 1891–1907'
author: 'Pentti Kokko'
date: '2018-12-27'
image: './hautuumaa-rajattu.jpg'
excerpt: 'Historian tutkiminen ja kirjoittaminen tuottaa usein ylimääräisiä tekstejä siinä mielessä, että ne mielenkiintoisuudestaan huolimatta jäävät pois lopullisesta teoksesta. Niin kävi minullekin, kun tein osaelämäkertaa Tuntematon kansakoulunopettaja isoisästäni Taavi Kokosta. Isoisäni elämää ja yhteiskunnallista toimintaa tutkiessani tunsin tutustuvani moneen hänen aikalaiseensa Ruskealassa. Luonnostelin ”kohtaamistani” henkilöistä joukon epätäydellisiksi jääneitä kertomuksia, koska houkuttelevienkin sivupolkujen ja -persoonien seuraaminen täytyy katkaista valitettavan ennenaikaisesti, jos tarkoitus on saada aikaan joskus jotain valmista. Jokainen ihminen on kuitenkin muistamisen arvoinen. Neiti Sofia Emilia Eklund ja koulumestari Aatami Suppanen ovat nostoja historiallisista tuttavistani Ruskealassa.'
disableSignature: true
keywords: 'Emma Eklund, Aatami Suppanen, Adam Suppanen, Aatto Suppanen, Maria Lovisa Suppanen, Mikko Auvinen, Ernest Ivar Roini, Juho Jalkanen, K. Basilier, T. Laurikainen, Taavi Kokko, Ruskeala, Pentti Kokko'
---

# Opettaja Taavi Kokon aikalaisia Ruskealassa 1891–1907

Historian tutkiminen ja kirjoittaminen tuottaa usein ylimääräisiä tekstejä siinä mielessä, että ne mielenkiintoisuudestaan huolimatta jäävät pois lopullisesta teoksesta. Niin kävi minullekin, kun tein osaelämäkertaa Tuntematon kansakoulunopettaja isoisästäni Taavi Kokosta. Isoisäni elämää ja yhteiskunnallista toimintaa tutkiessani tunsin tutustuvani moneen hänen aikalaiseensa Ruskealassa. Luonnostelin ”kohtaamistani” henkilöistä joukon epätäydellisiksi jääneitä kertomuksia, koska houkuttelevienkin sivupolkujen ja -persoonien seuraaminen täytyy katkaista valitettavan ennenaikaisesti, jos tarkoitus on saada aikaan joskus jotain valmista. Jokainen ihminen on kuitenkin muistamisen arvoinen. Neiti Sofia Emilia Eklund ja koulumestari Aatami Suppanen ovat nostoja historiallisista tuttavistani Ruskealassa.

## Neiti Emma Eklundin testamentti

Neiti Sofia Emilia Eklund oli syntynyt Ruskealan Särkisyrjässä 9.7.1828 ja kuollut siellä 69-vuotiaana 4.9.1897. Hän oli suullisesti testamentannut omaisuutensa jaettavaksi niin, että jos hänen omistamansa kiinteistö ei joutuisi perintöoikeuden perusteella jollekin tuntemattomalle sukulaiselle, niin siinä tapauksessa 5 000 markkaa (23 930 €/2018), mutta vastakkaisessa tapauksessa 3 000 markkaa (14 360 €) lankeaisi Ruskealan kunnalle ”Emma Eklundin rahaston” nimellä. Rahastoa on hoidettava pysyvästi yhdessä kansakoulun varojen kanssa siten, että sen korosta annetaan lukukausittain apurahoja ahkerille, köyhille koululapsille. Ruskealan seurakunnan urkukassa saa 8 000 markkaa (38 290 €), Sortavalan lyseo saa loput omaisuudesta eli arvoltaan noin 15 000 markkaa (71 790 €).

<figure> 
    <img src="sofia-eklund.jpg" alt="Sofia Eklundin hauta"> 
    <figcaption>
        Emma Eklundin hautapaasi Ruskealan hautausmaalla 10.5.2012. Kuva Pentti Kokko.
    </figcaption> 
</figure>

Ruskealan kunta, kansakoulujohtokunta ja seurakunta valvoivat etuaan Emilia Eklundin testamentti- ja perintöasiassa kihlakunnan oikeuden välikäräjillä 11.1.1898 Ruskealan käräjäkunnassa Ahin tilalla N:o 1 Kollitsan kylässä. Oikeus katsoi todistajia kuultuaan testamentin päteväksi kansakoulurahaston ja urkukassan osalta. Sukulaisten suhteen kihlakunnan oikeus totesi, että jos Ruskealan kunta ja kansakoulujohtokunta tahtovat nauttia hyväkseen testamentin suullista määräystä, tulee niiden saattaa se tiedoksi vainajan tunnetuille perillisille kopiolla tästä protokollasta ja tuntemattomille painattamalla tämä protokolla maamme sekä suomen- että ruotsinkielisiin sanomalehtiin kolme kertaa. Saatuaan edellä sanotulla tavalla tiedon testamentista moittikoot viitatut perilliset yön ja vuoden kuluessa testamenttia laillisessa järjestyksessä, jos arvelevat siihen syytä olevan ja tahtovat puhevaltansa säilyttää.

Samalla tavoin täytyi kirkkoraadin valvoa Ruskealan seurakunnan etua urkukassan suhteen testamentin täytäntöönpanossa. Ruisselällä asuva herastuomari Matti Eronen edusti kummassakin tapauksessa edunsaajia.

Emilian vanhemmat oli vihitty 27.10.1822 Elimäellä, missä sulhanen Karl Eklund oli Peippolan kartanon pehtori ja morsian Maria Helena Hymén kamarineito (ylhäisönaisen henkilökohtainen palvelija) Moision kartanossa. Karl Eklund kuoli tilanomistajana Ruskealassa 3.3.1872.
Maanviljelijän leskirouva kuoli 92-vuotiaana ”vanhuuteen” 14.8.1892.

Paikkakuntalaiset tunsivat Emma Eklundin äveriäänä ihmisenä. Naimattoman ja lapsettoman naisen omaisuudella olisi ollut ottajia ja siinä tarkoituksessa kaikkiin suupuheisiin tartuttiin ihka tosissaan. Välikäräjiä seuranneilla lakimääräisillä talvikäräjillä 3.2.1898, jotka pidettiin kauppias Iwan Ratisen omistamalla tilalla N:o 2 Otrakkalan kylässä, mökkiläinen Mikko Auvinen Pirttipohjan
kylästä kertoi, mitä neiti Eklund oli eläessään luvannut hänelle.

Torppari Taneli Paavonpoika Boehm, Sortavalan pitäjän Suuren Rytyn kylästä, ja talollisen vaimo Anna Hasunen o.s. Utunen Ruskealan Issakanvaaran kylästä, hyvämaineisina ja väärän valan vaaroista varoitettuina erikseen todistivat Emma Eklundin sanomisia Mikko Auvisesta syyskesällä 1894. Tuolloin kivinavettaa Emma Eklundin luona rakentamassa ollut Boehm oli kysynyt neidiltä, mihin hänen omaisuutensa joutuisi kuolemansa jälkeen, kun hänellä ei ollut perillisiä. ”Kuolemani jälkeen jätän Mikko Auviselle sen tilan [N:o 4 Pirttipohjan kylä], jolla hän nyt asuu, toiset tilat jääköötpä kenelle tahansa”, Eklund oli vastannut lisäten vielä, ”että tila saisi jäädä vaikka hänen ristilapsellensa Mikko Auvisen tytölle Iidalle muistiksi”. Paikalla ollut Anna Hasunen vahvisti Boehmin kertomuksen. Neiti Eklund, jonka palveluksessa Hasunen oli ollut 1891–1894, oli tänä aikana usein puhunut aikomuksestaan määrätä ko. tila Mikko Auviselle, ”mutta mitään täsmällistä määräystä ei hän näissä tilaisuuksissa ollut tehnyt; kerrottu ja omistettu”.

Oikeus ohjasi Mikko Auvista, että jos hän tahtoi neiti Sofia Emilia Eklundin suullista määräystä testamenttina valvoa, antamaan testamentti säädetyllä tavalla tiedoksi vainajan lähimmillesukulaisille. Sen tiedoksi saatuaan sukulaisilla oli yö ja vuosi aikaa moittia testamenttia, jos katsovat siihen olevan aihetta ja tahtovat säilyttää puhevaltansa. Oikeudessa Mikko Auvista avusti maanviljelijä Ernest Ivar Roini.

Mikko Auvinen todella kuulutti testamentista asianmukaisesti Ruskealan kunnan ja seurakunnan tavoin Suomalaisessa Wirallisessa Lehdessä ja Finlands Allmänna Tidningenissä. Miten Mikko Auvinen tai Ruskealan kunta ja sen kansakoulujohtokunta menestyivät testamentin valvonnassa, jäi minulta selvittämättä, mutta ainakin seurakunnan urkukassa karttui Sofia Emilia Eklundin omaisuudesta, vaikka kirkkoraati joutuikin tyytymään kompromissiin.

Kirkkoraati valtuutti toukokuussa 1902 puheenjohtajansa Juho Jalkasen hakemaan kuvernööriltä kieltoa pankkikomisarius K. Basilierille luovuttaa neiti Sofia Emilia Eklund-vainaan omaisuutta Eklundin oikeudenomistajille, ennen kuin vainajan tekemä testamentti Ruskealan kirkon urkujen hyväksi on saanut lainvoiman.

Ruskealan seurakunnan osalta Eklund-vainajan testamenttijuttu saatiin sovinnon kautta lopullisesti päätöksen vasta kesällä 1902 siten, että seurakunta oli saanut urkurahastoon vainajan jäämistöstä 5 000 markkaa (22 260 €/2018). Testamentin koko arvo oli 8 000 markkaa (35 620 €).

## Koulumestari, Ruskealan kunniavanhus Aatami Suppanen

Kirkkoraadin puheenjohtaja, rovasti Juho Jalkanen ilmoitti elokuussa 1896 raadille, että seurakunnan vanhalle koulumestarille alkaa käydä koulunpito liian rasittavaksi ja kysyi, eikö raati voisi keksiä mitään keinoa hänen vapauttamisekseen? Raati antoi tunnustusta Suppasen rehelliselle ja vilpittömälle työlle sekä sen tarpeellisuudelle myöntäen, että hän ansaitsisi saada helpotusta vanhuuden päivikseen, mutta kun seurakunta oli juuri silloin kovan kirkollisverotaakan rasittama, ei raati voinut ehdottaa Suppaselle eläkettä maksettavaksi. Koska hänen palkkansa oli niin pieni, ettei sillä voitu saada apulaista toimeen, raati toivoi, että Suppanen voisi vielä edes yhden vuoden hoitaa virkaansa.

<h3 style="text-align: center;"> Kirkkoraadin pöytäkirja 20.10.1898, 3 §</h3>

> Kun Kiertokoulunopettaja Aatami Suppanen, syntynyt 29/3 1820, tullut nykyiseen virkaansa 1842 virantoimittajaksi ja vakinaiseksi 1843, jota virkaa hän siis on 56 vuotta nuhteettomasti kiitettävällä ahkeruudella ja innolla sekä esikuvaksi kelpaavalla käytöksellä, koskaan nauttimatta virkavapautta, toimittanut, nyt korkean ikänsä vuoksi on käynyt virkaansa kykenemättömäksi ja koska hänen virkapalkkansa, joka tekee noin 10 tynnyriä rukiita ja korkeintaan 100 markkaa [465 €/2018] rahaa, on niin pieni, ettei hän, taloudelliseen ahdinkoon joutumatta voi ottaa osaa virantoimittajan palkkaamiseen, sekä koska seurakunta on yleiseen vähävaraisimpia Karjalassa ja nykyisin on rasitettu paitsi korkeilla kunnallis- ja vaivais-maksuilla, jotka vähävaraisissa seurakunnissa aina ovat suuremmat ja tuntuvammat kuin varakkaissa, lahjoitusmaiden lunastuksilla, joka rasitus noin 96 markkaa manttaalia kohden, painaa noin puolta seurakuntaa, ja nykyisin vireillä olevan Kirkkoherran virkatalon korjausmaksuilla, joka virkatalo nykyisen viranomistajan virkaan astuessa oli täydellisessä rappiotilassa, niin päätti seurakunnan Kirkkoraati kaikkein syvimmässä alamaisuudessa kääntyä Hänen Keisarillisen Majesteettinsa puoleen yhtä alamaisimmalla pyynnöllä, että Suomen Waltion varoista suotaisiin yllämainitulle Suppaselle 300 markan [1 395 €] suuruinen vuotuinen eläke hänen loppuijäksensä. Tätä anomusta Hänen Keisarilliselle Majesteetillensa Kirkkoraadin puolesta tekemään valtuutettiin allekirjoittanut puheenjohtaja [J. Jalkanen].

Kuolema ehti kuitenkin eläkkeen edelle. Sanomalehti Laatokka julkaisi koulumestari, kiertokoulunopettaja Aatami Suppasesta laajan muistokirjoituksen. Se on mitä ilmeisimmin Laatokan Ruskealan kirjeenvaihtajan, opettaja Taavi Kokon laatima. Kokko muisteli Aatami Suppasta vielä 1914 joulupakinassaan Mikkelin Sanomissa.

<h3 style="text-align: center;">Vainajan muistoksi</h3>

> Viime jouluaattona [1898] kuoli Ruskealassa kiertokoulun opett. Adam Suppanen äskettäin eläväin mailta poistuneen tunnetun kirjailijain Aatto S(uppase)n isä. Vainaja oli syntynyt 29.3.1820 Taipalsaarella, josta muutti sanottuun toimeensa Ruskealassa 1842 ja jossa hoiti tätä toimintaansa yhtämittaa 55 vuotta eli viime kesään asti, viljellen väliaikoina pientä mökin palstaansa kirkkoherran virkatalon maalla lähellä kirkkoa. Pitäjän lainakirjastoa hän hoiti pitemmän aikaa, oli myös jonkun aikaa kansakoulun johtokunnan jäsenenä.

> Se ulkonainen kehys, jonka piirissä vainajan elämä siis liikkui, ei suinkaan suurekas ollut. Olihan vaan, kuten monen hänen vertaisensa valistuksen siemenen kylväjän kansan syvien rivien keskuudessa, ”maailman silmiss´ arvoton”. Jollemme ota lukuun tuota tavattoman pitkää työkautta samassa, mitä niukimmin palkatussa toimessa, niin ei se sinänsä moninaiseen muisteluun aihetta anna.
> Siihen nähden kuitenkin, jotka vainajaa tarkemmin tunsivat – vai lieneekö sanottava: pystyivät tuntemaan – oli hän harvinainen ilmiö ihmislasten suuressa joukossa; ilmiö, josta nykyisellä nousevalla nuorisolla, kasvavalla kansalla olisi paljon oppimista. Lienee sen vuoksi suvaittua useampikin muistosana tuosta muuten asemaltaan arvottomasta, manan majoille, ei vainenkaan ”paremmille jouluille” menneestä vanhuksesta.

> Hänen lapsuutensa ja nuoruutensa aikana ei varsinkaan suomalaisille koulutietoja enempi kuin taitojakaan liioin tarjottu. Omin neuvoin oli hänkin oppinut, minkä oppinut oli. Ja joskin ne puitteet, joiden piiriin hänen ulkonainen, yhteiskunnallinen toimintansa rajoittui, olivat ahtaat – laveampia ei hän halunnutkaan – niin oli hänen sisällinen elämänsä, sielunsa kehys sitä tilavampi. Ukko nähtävästi karttoi päärlyjensä heittämistä sikain eteen ja sen vuoksi ei hän jokaista mielellään päästänyt kurkistelemaan henkensä rikkaimpiin [?]. Mutta joihin hänellä oli luottamusta, joiden hän toivoi itseänsä käsittävän, ne saivat aihetta kunnioittavaan ihmettelyyn kuullessaan, mitä elämän viisautta tuon – ulkomuodoltaankin kyllä juhlallisen kunnioitusta herättävän harmaahapsisen karkean kuoren alla löytyi. Mitä erilaisempiin elämän ilmiöihin, kohtaloihin ja suhteisiin hänellä oli aina hyvin tyydyttävä, sattuvalla vertauksella valaistu selitys varustettuna. Verrattain runsaalla kirjallisuuden, erittäinkin P. Raamatun viljelemisellä ja oman uutteran ajatustoimensa kautta oli hän tämän rikkaan sisällyksen hengellensä hankkinut. ”Ihminen ei saa yhtään hetkeä ajastaan viettää turhaan”, olikin hänellä tapana sanoa. ”Jollei hänellä ole muuta askaroimista, niin tulee edes ajatella.”

> Usein tapaa tämmöisissä itseoppineissa ajattelijoissa mitä suurinta itsetyytymystä, josta johtuu koulujen ja toisten ihmisten ajatusten halveksimista. Mutta puheenalainen vanhus osoitti mitä herkintä korvaa muidenkin ajatuksille ja koulutiedoille ja kunnioitti ehkä liiaksikin (katso isä Marttia kirjoituksessa: ”Mökin Poika” äsken ilmaantuneessa ”Kirjailija-albumista”.) Mielellään hänen kanssaan puheli kaikellaisista asioista ja aatteista.
> Tämän maailman hyvyyteen nähden olivat vainajan toimeentulonehdot niukat. Suurimmalla uutteruudella ja tarkimmalla säästäväisyydellä sitä siksi saatiin kuitenkin kokoon, että voitiin evästää lapsia kouluun, jota silloin ei vielä oman mökin nurkan juurella löytynytkään. Tässä suhteessa noudatti ukko ohjetta: ”Kootkaa murut, ettei mitään hukkuisi.” Ja tyytyväinen, vakavan iloinen, jopa leikkisäkin mieliala näkyi ukossa vallinneen, milloin vain hänen pakeilleen tuli. Paha kuten hyväkin päivä otettiin yhtä levollisena vastaan. Kun esim. viime talvena saapui sanoma tuon hänen nähtävästi sangen rakkaan poikansa, Aatto Suppasen kuolemasta, ja joku hänen ystävistään, luullen, ettei tämä siitä vielä tiedä, koetti kierrellen saada asiaa ukolle sanotuksi, pääsi tältä tavallinen leveähkö nauru: ”Ha, ha, ha; sanokaa vaan suoraan, että se on kuollut”. Mutta eikö tämä ollut tuota luonnonlapsen tunteettomuutta? Ei; kyllä se oli sisällisen itsekasvatuksen kautta saavutettua miehuutta.

> Niinä aikoina, jolloin hän opettaja-urallensa valmistautui, samoin hänen opettajatoimensa viehkeimpinä aikoina, ei varsinkaan kiertokoulujen alalla tunnettu uudenaikaisia opetus-opillisia menettelytapoja. Näitä oli hänkin puuttunut, ja siinä lienee ollut hänen opettajatoimensa heikoin puoli. Kumminkin tuntuu seurakuntalaisissa, jotka melkein järestään nuoremmista vanhempiin asti ovat hänen oppilainaan olleet, asuvan syvä kunnioitus tuota vanhaa opettajaansa, ”hyvien neuvojen antajaa” kohtaan.

> Vielä viime syksynä oli ukolla mieli mennä tointansa jatkamaan, mutta hänen heikontuneiden voimiensa tähden katsoi asianomainen kirkkoneuvosto hänen jo tarvitsevan lepoa ja hankki sijalle vereksen miehen. Mielihyvällä kuuli ukko sanoman tästä, mutta silloin tuli kuolontautikin ”ikään kuin pehkon takana odotteleva” kohta kiinni saaliiseensa. Tapahtui siis kuten sotilaalle, joka vaikka, vuotavista haavoistaan yhä heikontuikin, edessään olevan tehtävän suorittamisesta jännittyneenä pitää ryhtinsä, pysyy pystyssä, mutta raukeaa ja kaatuu kohta, kun hänen sijansa rivissä vereksillä voimilla täytetään ja hän saa väistyä.
> Kivuton elintoiminnan taukoaminen oli ukon tauti. Leikkisä puhe tuosta taudistansa vierähti vielä tuontuostakin hänen huuliltaan. Kuolema, kuten elämäkin oli hiljainen kristityn kuolema, täynnä elämän toivoa. – Lähinnä surevat hänen kuolemastaan ”kilvassa” jälelle jäänyt leski, äsken (2/11 95), kultahäänsä viettänyt morsian, sekä kaksi tytärtä.

> Levätköön rauhassa leiviskänsä uskollisesti hoitanut vanhus!

> Ehdottomasti tuon vainajan, Adam Suppasen, kuvaa muistellessa, joutuu ajattelemaan, että siinä oli jalokivi, joka vaan aikansa olisi tarvinnut tulla asianmukaisissa laitoksissa tahkotuksi voidakseen loistaa kansansa etevimpien riveissä. Toiselta puolen tulee ajatelleeksi, että eiköhän noin täyteläisin tähkäpäin tuleentunutta hengen-viljaa voisi tuonen niittomies tavata taajemmassakin, kun me nuoret yhtä tarkkatuntoisesti kuin hän nähtävästi oli tehnyt, voisimme varjella sydämemme, henkemme vainion sinne tuhotöille tunkevilta kaikellaisilta hornanhengiltä.” [Taavi Kokko oli 34 vuotta Adam Suppasen kuollessa 79-vuotiaana.]

Kymmenen kuukautta myöhemmin 2.11.1899 kuoli Ruskealassa koulumestari Suppas-vainajan leski Maria Lovisa Suppanen o.s. Kettunen 80 vuoden iässä. Laatokan Ruskealan kirjeenvaihtaja kirjoitti, että ”[v]ainaja oli saanut eläessään paljon kovaa kokea, nähdä puutetta ja vaivaa suuren ja vähävaraisen perheen äitinä, mutta voimainsa mukaan koitti hän ynnä miehensä vähillä varoilla kasvattaa ja kouluttaa lapsensa, joista vanhin köyhästä kodista evästettiin yliopistoon asti. Rauhassa levätköön kunnon vanhus.”

### Hauta- ja muistomerkki

Hauta- ja samalla muistopatsas pystytettiin Adam Suppaselle Ruskealan hautausmaalla jouluaattona 1901. Sirotekoisen marmoripatsaan kupeessa oli luettavana mm. sanat: Kiitollisilta oppilailta”.

Tilaisuudessa, johon oli saapunut vainajan ystäviä, oppilaita ja omaisia, puhuivat maanviljelijä T. Laurikainen ja rovasti J. Jalkanen. Edellinen muisteli oppilaana tältä vaatimattomalta, uutteralta opettajaltaan saamia opetuksia ja niiden vaikutuksia. Jälkimmäinen taas teroitti mieliin vainajan tärkeää merkitystä korvenraivaajana valistustyön alalla.

<div class="post__signature">
- Pentti Kokko<br/>
Ruskiilainen 2018<br/>
Pääkaupungin Ruskeala-Seura ry:n perinnejulkaisu No 3<br/>
Seuran 50-vuotisjuhlajulkaisu<br/>
</div>
