import React, { Fragment } from 'react'
import Helmet from 'react-helmet'

import './typography.css'
import './base.css'
import './utils.css'
import favicon from '../static/img/favicon.ico'

const Layout = ({ children }) => {
  return (
    <Fragment>
      <Helmet
        title="Maisteri Pentti Kokko"
        htmlAttributes={{
          lang: 'fi',
        }}
      >
        <link
          href="https://fonts.googleapis.com/css?family=Libre+Baskerville|Special+Elite"
          rel="stylesheet"
        ></link>
        <link rel="shortcut icon" href={favicon} type="image/x-icon" />
        <meta
          name="Description"
          content="Valtiotieteen maisteri Pentti Kokko on syntynyt Mikkelissä vuonna 1947 ja valmistunut Helsingin yliopistosta 1971 pääaineenaan kansantaloustiede. Kokko toimi tilasto- ja pankkialalla 1970-2010. Historian Ystäväin Liiton hallituksen jäsen hän on ollut vuodesta 2011."
        />
        <meta
          name="keywords"
          content="Pentti Kokko, Taavi Kokko, Tuntematon kansakoulunopettaja, Luumäki, Mikkeli, Ruskeala"
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="theme-color" content="#151515" />
        <meta name="msapplication-navbutton-color" content="#151515" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta
          name="apple-mobile-web-app-status-bar-style"
          content="black-translucent"
        />
        <meta
          name="google-site-verification"
          content="2CYG861iGHLL-0MvIie-7oawnpGJBGZu326N96rWrnI"
        />
      </Helmet>
      <Fragment>{children}</Fragment>
    </Fragment>
  )
}

export default Layout
