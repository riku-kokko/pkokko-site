import React from 'react'
import './blog-post.css'
import 'animate.css/animate.min.css'
import './parallax.css'
import ScrollAnimation from 'react-animate-on-scroll'
import Layout from '../layout'
import { graphql } from 'gatsby'
import Helmet from 'react-helmet'

export default class BlogPost extends React.PureComponent {
  render() {
    const post = this.props.data.markdownRemark
    return (
      <Layout>
        <Helmet>
          <meta name="keywords" content={post.frontmatter.keywords} />
        </Helmet>
        <div className="parallax">
          <div className="parallax__layer parallax__layer--deep post__hero-container">
            <img
              src={post.frontmatter.image.childImageSharp.resize.src}
              className="post__hero"
              alt={post.frontmatter.image.name}
            />
          </div>
          <div className="parallax__layer parallax__layer--back">
            <a href="/">
              <h2 className="post__title">Maisteri Pentti Kokko</h2>
            </a>
          </div>
          <div className="parallax__layer parallax__layer--base">
            <div className="post__container">
              <ScrollAnimation animateIn="fadeIn" animateOnce={true}>
                <div className="post__content">
                  <div dangerouslySetInnerHTML={{ __html: post.html }} />
                  {!post.frontmatter.disableSignature && (
                    <div className="post__signature">
                      -{post.frontmatter.author}, {post.frontmatter.date}
                    </div>
                  )}

                  <div className="post__link-to-frontpage">
                    <a href="/">
                      <span className="arrow arrow-left" />
                      <span>Etusivulle</span>
                    </a>
                  </div>
                </div>
              </ScrollAnimation>
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

export const query = graphql`
  query BlogPostQuery($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
        author
        date(formatString: "DD.MM.YYYY")
        disableSignature
        keywords
        image {
          name
          childImageSharp {
            resize(width: 900, grayscale: true) {
              src
            }
          }
        }
      }
    }
  }
`
