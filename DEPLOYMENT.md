# Deployment

## Initial setup

- Install terraform
- Create `./terraform.tfvars`
  ```
  aws_access_key = "key-value"
  aws_secret_key = "secret-value"
  ```
- Run `terraform init`

## Deployment after initial setup

- Run `./deploy.sh`
